![alt text](Image/Software_Requirements_Specification_for_Sistem_Pengisian_Logbook_PCR_Page_1_page-0001.jpg?raw=true)


<!-- <div align="center">

# Sistem Pengisian Logbook PCR
<img width="500" src="Image/Politeknik_Caltex_Riau.png" alt="PCR">

</div> -->



# BAB I Pendahuluan

## 1.1 Tujuan

Dokumen Software Requirement Spesification (SRS) merupakan dokumen spesifikasi perangkat lunak untuk membangun "Sistem Pengisian LogBook".Dokumen ini dibangun untuk memudahkan mahasiswa dalam mengisi logbook dan juga memudahkan kepala lab untuk melihat kondisi lab.

## 1.2 Lingkup

Sistem Pengisian LogBook merupakan Sistem yang kami bangun untuk mempermudah kepala laboratorium dalam melihat kondisi laboratorium,memudahkan admin dalam melihat data-data nya dan juga memudahkan mahasiswa dalam mengisi logbook.

## 1.3 Akronim, singkatan, definisi

| Istilah                            | Definisi                                                                                     |
| ---------------------------------- | -------------------------------------------------------------------------------------------- |
| SRS                                | Software Requirement Specification                                                           |
| Login                              | Digunakan untuk mengakses aplikasi                                                           |
| Software Requirement Specification | perangkat lunak yang akan dibuat dan sebagai penyembatani komunikasi pembuat dengan pengguna |
| Use Case                           | situasi dimana sistem anda digunakan untuk memenuhi satu atau lebih kebutuhan pemakaian anda |

## 1.4 Referensi
Referensi yang digunakan dalam pengembangan perangkat lunak ini adalah :

* https://elib.unikom.ac.id/files/disk1/690/jbptunikompp-gdl-dhanielpra-34490-1-unikom_d-l.pdf

## 1.5 Overview

Bab selanjutnya yaitu menjelaskan sistem yang di terapkan pada aplikasi. Menjelaskan gambaran umum dari aplikasi, sistem interface aplikasi dan alur sistemnya. Bab terakhir menjelaskan tentang setiap fungsi yang digunakan secara teknisnya. Pada bab 2 dan 3 merupakan deskripsi dari aplikasi yang akan diterapkan pada aplikasi yang dibuat.

---

# BAB II Gambaran umum

Pada zaman era globalisasi perkembangan teknologi begitu sangat pesat, salah satunya ialah perkembangan teknologi di bidang software engineering dimana software engineering dapat digunakan dalam kehidupan sehari - hari .dalam studi kasus Proyek II ini kami menganalisis kebutuhan kampus dalam pengisian logbook kegiatan di Laboratorium. kasus yang kami peroleh yaitu Sistem Pengisian Logbook Kampus. Maka dari itu kami sebagai software engineering merancang sebuah sistem sesuai dengan kebutuhan Kepala Laboratorium dan Asisten Instruktur Laboratorium dengan menerapkan Sistem Pengisian Logbook Kampus. Sehingga memudahkan Mahasiswa mengisi logbook dan Memudahkan pemantauan kegiatan yang dilakukan di Laboratorium. Software yang kami buat ini berbasis website dimana website sebagai admin itu AIL(Asisten Instruktur Laboratorium) Super Admin sebagai Kepala Laboratorium. Sistem yang kami buat di dalamnya terdapat logbook, laporan(untuk Kepala Laboratorium). Berikut akan kami jelaskan sistem software kami, Mahasiswa fungsi utama yaitu :

- Input Nama
- Input NIM
- Input Kelas
- Input Dosen/AIL
- Input Matkul
- Input Kondisi Peralatan Lab

Berikut ini fungsi Admin:

- View Data Logbook
- Input Laporan Kerusakan

Berikut ini fungsi Super Admin:

- Input Data Lab
- Input Data User
- Edit Data Lab
- Delete Data Lab
- View Data Lab
- Cetak Laporan

## 2.1 Perspektif Produk

Sistem Pengisian LogBook adalah sebuah sistem untuk mengisi logbook yang diaplikasikan pada website.Terdapat 3 jenis yaitu Kepala Laboratorium sebagai SuperAdmin,AIL(Asisten Instruktur Laboratorium) sebagai Admin dan Mahasiswa sebagai User.Pengelolaan data dikelola oleh Kepala Laboratorium pada website,Admin hanya bisa mengisi dan melihat data-data logbook dan Mahasiswa hanya bisa mengisi logbook.

### 2.1.1 Antarmuka pengguna

* ![alt text](Image/Landing Page.png?raw=true)


### 2.1.2 Antarmuka Perangkat Keras
![alt text](Image/AntarmukaPerangkatKeras.png?raw=true)

Antarmuka perangkat keras yang digunakan untuk mengoperasikan Perangkat Lunak Sistem Pengisian LogBook Laboratorium antara lain :
1. PC / Laptop untuk menjalankan Aplikasi ini admin membutuhkan sebuah PC yang menggunakan OS Windows,Linux, atau MAC dan sudah terinstall browser.

### 2.1.3 Antarmuka perangkat lunak

Tidak ada

### 2.1.4 Antarmuka Komunikasi

Antarmuka komunikasi yang digunakan untuk mengoperasikan Perangkat Lunak Sistem Pengisian Logbook PCR antara lain :

1. Kabel Lan UTP RJ45 <br> 
2. Modem <br> 
3. Wifi

### 2.1.5 Batasan memori

Tidak ada

### 2.1.6 Operasi-Operasi
| Operasi | Fungsi |
| ------ | ------ |
| Login | Digunakan untuk mengakses aplikasi |
| Input Data logbook | Digunakan untuk memasukkan data-data logbook |
| Kembali | Digunakan untuk kembali ke halaman sebelumnya |
| Hapus | Digunakan untuk menghapus data |
| Edit | Digunakan untuk mengubah data |
| View | Digunakan untuk menampilkan data |
| Simpan | Digunakan untuk menyimpan data |
| Cetak | Digunakan untuk mencetak laporan |

### 2.1.7 Kebutuhan adaptasi

Tidak ada

---

## 2.2 Spesifikasi Kebutuhan Fungsional
![alt text](Image/UseCaseInti.png?raw=true)

### 2.2.1 Kepala Laboratorium Login
UseCase:login<br>
Diagram:![alt text](Image/usecase_kepala_lab_login.png?raw=true)<br>
Deskripsi singkat Kepala Laboratorium melakukan login sebelum masuk ke tampilan home.<br>
Deskripsi Langkah-Langkah:<br>

1. Kepala Laboratorium melakukan login<br>
2. Sistem melakukan validasi<br>
3. Bila validasi berhasil akan mengarahkan ke halaman beranda<br>
4. Bila gagal sistem akan menampilkan peringatan<br>

### 2.2.2 Dosen AIL melakukan Login
UseCase:login<br>
Diagram:![alt text](Image/Dosen_AIL_Melakukan_Login.png?raw=true)<br>
Deskripsi singkat Dosen AIL melakukan login sebelum masuk ke tampilan home.<br>
Deskripsi Langkah-Langkah:<br>

1. Dosen AIL melakukan login<br>
2. Sistem melakukan validasi<br>
3. Bila validasi berhasil akan mengarahkan ke halaman beranda<br>
4. Bila gagal sistem akan menampilkan peringatan<br>

### 2.2.3 Mahasiswa melakukan Login
UseCase:login<br>
Diagram:![alt text](Image/Mahasiswa_Login.png?raw=true)<br>
Deskripsi singkat mahasiswa melakukan login sebelum masuk ke tampilan home.<br>
Deskripsi Langkah-Langkah:<br>

1. Mahasiswa melakukan login<br>
2. Sistem melakukan validasi<br>
3. Bila validasi berhasil akan mengarahkan ke halaman beranda<br>
4. Bila gagal sistem akan menampilkan peringatan<br>

### 2.2.4 Kepala Laboratorium Mengelola User
UseCase:Mengelola User<br>
Diagram:![alt text](Image/Kepala_lab_mengelola_user.png?raw=true)<br>
Deskripsi singkat Kepala Lab mengelola user.<br>
Deskripsi langkah-langkah:<br>

1.Kepala Lab mengklik manajemen user,lalu kepala lab bisa melihat user,menghapus user,menambah user dan mengedit user<br>
2.Sistem akan menyimpan data user dan akan menampilkan data user yang dipilih<br>
3.Kepala Lab bisa menghapus hak akses user<br> 

### 2.2.5 Kepala Laboratorium Mengelola Dashboard
UseCase:Dashboard<br>
Diagram:![alt text](Image/Kepala_lab_mengelola_dashboard.png?raw=true)<br>
Deskripsi singkat Kepala Laboratorium mengelola Dashboard.<br>
Deskripsi langkah-langkah:<br>

1.Kepala lab masuk ke halaman dashboard SuperAdmin<br>
2.Sistem menampilkan halaman dashboard SuperAdmin<br>
3.Kepala lab dapat Melihat data dan dapat menambah,mengedit atau menghapus data.<br>
4.Sistem melakukan validasi jika data sudah ada.<br>


### 2.2.6 Kepala Laboratorium Mencetak Laporan
UseCase:Laporan<br>
Diagram:![alt text](Image/Kepala_lab_cetak_laporan.png?raw=true)<br>
Deskripsi singkat Kepala Laboratorium mencetak laporan<br>
Deskripsi langkah-langkah:<br>

1.Kepala Lab masuk ke halaman laporan laboratorium<br>
2.Sistem menampilkan halaman laporan laboratorium dan menampilkan hasil laporan<br> 
3.Kepala Laboratorium mencetak laporan<br>


### 2.2.7 Kepala Laboratorium Mengelola LogBook
Usecase:Mengelola logbook<br>
Diagram:![alt text](Image/Kepala_lab_mengelola_logbook.png?raw=true)<br>
Deskripsi singkat Kepala Laboratorium mengelola LogBook<br>
Deskripsi langkah-langkah:<br>

1.Kepala Laboratorium masuk ke halaman SuperAdmin<br>
2.Sistem menampilkan halaman SuperAdmin<br>
3.Kepala Laboratorium melihat data logbook<br>


### 2.2.8 Dosen AIL mengelola logbook
UseCase:Mengelola LogBook<br>
Diagram:![alt text](Image/Dosen_AIL_Mengelola_LogBook.png?raw=true)<br>
Deskripsi singkat Dosen AIL mengelola logbook<br>
Deskripsi langkah-langkah:<br>

1.Dosen AIL masuk ke halaman admin<br>
2.Sistem menampilkan halaman admin<br>
3.Dosen AIL mengisi logbook dan melihat data logbook<br>


### 2.2.9 Mahasiswa mengisi logbook
USeCase:Mengisi LogBook<br>
Diagram:![alt text](Image/mahasiswa_mengisi_logbook.png?raw=true)<br>
Deskripsi singkat mahasiswa mengisi logbook<br>
Deskripsi langkah-langkah:<br>

1.Mahasiswa masuk ke halaman user<br>
2.Sistem menampilkan halaman user<br>
3.Mahasiswa mengisi data logbook dan mengirim atau menyimpan data ke sistem logbook.<br>
4.Sistem akan menyimpan data logbook yang telah di isi.<br>


## 2.3 Spesifikasi Kebutuhan non-fungsional

Tabel Kebutuhan Non-fungsional
| No | Deskripsi |
| ------ | ------ |
| 1 | Semua interface dan fungsi menggunakan Bahasa Indonesia |
| 2 | Perangkat Lunak dapat dipakai di semua platform OS ( AIL,Kepala Laboratorium dan Mahasiswa) |

## 2.4 Karakteristik Pengguna

Karakteristik pengguna dari perangkat lunak ini adalah pengguna langsung berinteraksi dengan sistem tanpa harus dihubungkan dengan hak akses atau level autentikasi.

## 2.5 Batasan - batasan

* Perangkat lunak web hanya dijalankan di windows (10, 11).
* Waktu pengembangan perangkat lunak yang singkat membuat adanya kemungkinan tidak semua fungsi yang ada dapat dilaksanakan.

## 2.6 Asumsi - Asumsi

Maksimal penginputan id atau memasukkan kode pada aplikasi ini adalah 9999, lebih dari itu program akan muncul peringatan"Anda telah melebihi batas maksimum".

## 2.7 Kebutuhan Penyeimbang

Tidak ada

---

# BAB III Requirement Spesification

## 3.1 Persyaratan Antarmuka Eksternal

Salah satu cara mengakases aplikasi ini yaitu dengan hak akses yang di berikan oleh Kepala Laboratorium, login melalui aplikasi ini dengan mencantumkan username kemudian sistem akan mencocokkan username Kepala Laboratorium dan Laboratorium. Setelah login berhasil Kepala Laboratorium dapat melihat kegiatan yang dilakukan di laboratorium dan dapat mencetak laporan perminggu dan perbulan.

## 3.2 Functional Requirement

Logika Struktur terdapat pada bagian 3.3.1

### 3.2.1 Kepala Laboratorium Login

| Nama fungsi    | Login                                                                                                                                                                                                                                                                                |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Xref           | Bagian 2.2.1, Login Kepala Lab                                                                                                                                                                                                                                                       |
| Trigger        | Membuka aplikasi Sistem Pengisian LogBook        Lab                                                                                                                                                                                                                                    |
| Precondition   | Halaman login                                                                                                                                                                                                                                                                        |
| Basic Path     | 1. Kepala Laboratorium mengisi form login dengan username dan password <br> 2.Kepala Laboratorium mengklik tombol login <br> 3. Sistem melakukan validasi login <br> 4. Bila sukses sistem akan mengarahkan ke halaman beranda <br> 5. Bila gagal sistem akan menampilkan peringatan |
| Alternative    | Tidak ada                                                                                                                                                                                                                                                                            |
| Post Condition | Kepala Laboratorium dapat login dan mengakses aplikasi MSistem Pengisian LogBook                                                                                                                                                                                                     |
| Exception Push | Username dan password salah                                                                                                                                                                                                                                                          |

### 3.2.2 AIL login

| Nam Fungsi     | Login                                                                                                                                                                                                           |
| -------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Xref           | Bagian 2.2.2 Login admin                                                                                                                                                                                        |
| Trigger        | Membuka aplikasi Sistem Pengisian LogBook    Lab                                                                                                                                                                   |
| Precondition   | Halaman login AIL                                                                                                                                                                                               |
| Basic Path     | 1. AIL melakukan login dengan username dan password <br> 2. Sistem melakukan validasi login <br> 3. Bila sukses sistem akan mengarahkan ke halaman beranda <br> 4.Bila gagal sistem akan menampilkan peringatan |
| Alternative    | Tidak ada                                                                                                                                                                                                       |
| Pos Condition  | AIL berhasil login dan mengakses Sistem Pengisian LogBook                                                                                                                                                       |
| Exception Push | Username dan password salah                                                                                                                                                                                     |

### 3.2.3 Mahasiswa login
| Nama Fungsi | login |
| ------ | ------ |
| Xref      | Bagian 2.2.3 Mahasiswa login       |
| Trigger      | Membuka aplikasi Sistem Pengisian LogBook Lab        |
| Precondition | Halaman login |
| Basic Path | 1. Mahasiswa mengisi form login dengan username dan password <br> 2. Mahasiswa mengklik tombol login <br> 3. Sistem melakukan validasi login <br> 4. Bila sukses sistem akan mengarahkan ke halaman beranda <br> 5. Bila gagal sistem akan menampilkan peringatan |
| Alternative | Tidak ada |
| Post Condition | Mahasiswa dapat login dan mengakses aplikasi Sistem Pengisian LogBook |
|Exception Push | Username dan password salah |

### 3.2.4 Kepala Laboratorium Mengelola User
| Nama Fungsi | Mengelola User |
| ------ | ------ |
| Xref     | Bagian 2.2.4, Mengelola User       |
| Trigger       |  Membuka aplikasi Sistem Pengisian LogBook Lab            |
| Precondition | Halaman utama Kepala Laboratorium |
| Basic Path | 1. Sistem menampilkan form. <br> 2. Kepala Laboratorium mengklik manajemen user, lalu kepala lab bisa melihat user, menghapus user,menambah user dan mengedit <br> 3. Sistem akan menyimpan data user ke database |
| Post Condition | Halaman user |
| Exception Push | Tidak ada koneksi, data user ke database |

### 3.2.5 Kepala Laboratorium Mengelola Dashboard
| Nama | Mengelola Dashboard |
| ------ | ------ |
| Xref       | Bagian 2.2.5, Mengelola Dashboard        |
| Trigger       | Membuka Sistem Pengisian LogBook Lab            |
| Precondition | Halaman utama Kepala Laboratorium |
| Basic Path | 1. Sistem menampilkan halaman dashboard SuperAdmin <br> 2. Kepala lab  dapat melihat data dan menambah, mengedit atau menghapus data <br> 3. Sistem melakukan validasi jika data sudah ada |
| Post Condition | Halaman SuperAdmin |
| Exception Push |  Tidak ada koneksi, data belum diinput |

### 3.2.6 Kepala Laboratorium Mencetak Laporan
| Nama Fungsi | Laporan |
| ------ | ------ |
| Xref       | Bagian 2.2.6 Cetak Laporan       |
| Trigger      | Membuka aplikasi Sistem Pengisian LogBook Lab       |
| Precondition | halaman utama Kepala Lab |
| Basic Path | 1. Kepala Lab mengklik tombol laporan <br> 2. Sistem menampilkan halaman laporan Laboratorium dan menampilkan hasil Laporan <br> 3. Kepala Laboratorium mencetak laporan |
| Alternative | Tidak ada |
| Pos Condition | Halaman Laporan |
| Exception Push | Tidak ada koneksi, data belum diinput |

### 3.2.7 Kepala Laboratorium Mengelola LogBook
| Nama Fungsi | Laporan |
| ------ | ------ |
| Xref       | Bagian 2.2.7 Mengelola LogBook       |
| Trigger      | Membuka aplikasi Sistem Pengisian LogBook Lab       |
| Precondition | halaman utama Kepala Lab |
| Basic Path | 1. Sistem menampilkan halaman SuperAdmin <br> 2. Kepala Laboratorium melihat data logbook |
| Pos Condition | Halaman Laporan |
| Exception Push | Tidak ada koneksi, data belum diinput |

### 3.2.8 AIL Mengelola LogBook
| Nama Fungsi | Laporan |
| ------ | ------ |
| Xref       | Bagian 2.2.8 Mengelola LogBook       |
| Trigger      | Membuka aplikasi Sistem Pengisian LogBook Lab       |
| Precondition | halaman utama  AIL |
| Basic Path | 1. Sistem menampilkan halaman Admin <br> 2. AIL melihat data logbook |
| Pos Condition | Halaman Laporan |
| Exception Push | Tidak ada koneksi, data belum diinput |

### 3.2.9 Mahasiswa Mengisi LogBook
| Nama Fungsi | Laporan |
| ------ | ------ |
| Xref       | Bagian 2.2.9 Mengisi LogBook       |
| Trigger      | Membuka aplikasi Sistem Pengisian LogBook Lab       |
| Precondition | halaman utama  user |
| Basic Path | 1. Sistem menampilkan halaman user <br> 2. mahasiswa mengisi data logbook dan mengirim atau menyimpan data ke sistem logbook <br> 3. Sistem akan menyimpan data logbook yang telah diisi |
| Pos Condition | Halaman Laporan |
| Exception Push | Tidak ada koneksi, data belum diinput |

---

## 3.3 Struktur Detail Kebutuhan Non-fungsional

### 3.3.1 Logika Struktur Data

Struktur data logika pada sistem pengisian logbook dijelaskan menggunakan ERD.
![alt text](Image/ERDFix1.jpg?raw=true)

Tabel User
| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
| Id_pc    | int | Nomor auto_increment Id_pc |
| Username  | varchar  | Berisi email untuk akses login |
| Password | varchar | Berisi password untuk login |
| Level | varchar | untuk membedakan level saat login |

Tabel pc
| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
| Id_pc    | int | foreign key tabel user |
| nomor_pc  | int  | Berisi nomor pc di lab |

Tabel logKegiatan
| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
| Id_logke    | int | Nomor auto_increment Id_logke |
| nama  | varchar  | Berisi email untuk akses login |
| nim | varchar | Berisi Nim yang mengisi logkegiatan  |
| nohp | varchar | Berisi nohp yang mengisi logkegiatan |
| kelas | varchar | untuk kelas yang mengisi logkegiatan |
| email | varchar | berisi email untuk login |
| kegiatan | varchar | berisi isi kegiatan nya |
| jamMasuk | time | jam masuk kegiatan |
| jamKeluar | time | jam keluar kegiatan |
| tanggalMulai | date | tanggal dimulai kegiatan |
| tanggalSelesai | date | tanggal selesai kegiatan |
| alat | varchar | alat yang digunakan |
| peserta | varchar | berisi peserta dari kegiatan tersebut |
| keterangan | varchar | keterangan kegiatan |

Tabel Laboratorium
| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
| Id_lab    | int | Nomor auto_increment Id_lab |
| ruang_lab  | int  | Berisi ruang lab yang ingin digunakan |

Tabel logKuliah
| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
| Id_logkul   | int | Nomor auto_increment Id_logkul |
| nama  | varchar  | Berisi nama yang ada dalam kegiatan perkuliahan |
| nim | varchar | Berisi nim yang ada dalam kegiatan perkuliahan  |
| kelas | varchar | untuk kelas yang digunakan |
| alat | varchar | Berisi alat yang digunakan |
| jamMasuk | time | jam masuk perkuliahan |
| jamKeluar | time | jam keluar perkuliahan |
| keterangan | varchar | keterangan perkuliahan |
| dosen | varchar | nama dosen di kegiatan perkuliahan|
| matkul | varchar | nama matakuliah |
| monitor | varchar | kondisi monitor |
| keyboard | varchar | kondisi keyboard |
| mouse | varchar | kondisi mouse |
| jaringan | varchar | kondisi jaringan |
| sks | int | Berisi sks perkuliahan |
| hadir | varchar | Berisi nama yang hadir di perkuliahan |
| tidak hadir | varchar | Berisi nama yang tidak hadir di perkuliahan |








